import {FlatList, TextInput, View} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {setFooter, setHeaderLeftButton, setHeaderRightButton} from 'headerFooter';
import {navigate, getPreviousRouteName, registerRoute} from 'router';
import {
    Icon,
    mainInputStyle,
    ItemList,
    FormattedText,
    CircledImage,
    EmptyData, LoadMore, AppContainer
} from 'elements';

import {getLastActivity, setPagination, shouldKeepRepoFooter} from 'utils';
import {getCommits} from './selectors';
import {clearCommits, requestCommits} from './actions';
import {Footer} from '../repository/Footer';

class CommitsComponent extends Component {
    constructor() {
        super();

        this.state = {searchText: '', isSearchVisible: false, filteredData: []};
    }

    componentDidMount() {
        const {requestCommits, setHeaderRightButton, setHeaderLeftButton, setFooter} = this.props;

        requestCommits();
        setHeaderRightButton(this.headerRightButton());
        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
    }

    headerRightButton() {
        return <Icon name={'search'} size={30} onPress={() => this.toggleSearch()}/>;
    }

    headerLeftButton() {
        return <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => this.props.navigate('Repository')}/>;
    }

    footer() {
        const {previousRouteName} = this.props;

        return shouldKeepRepoFooter(previousRouteName) ? <Footer/> : null;
    }

    componentWillUnmount() {
        this.props.clearCommits();
        this.props.setPagination('commits', {'x-page': 0});
    }

    toggleSearch() {
        const {isSearchVisible} = this.state;
        this.setState({isSearchVisible: !isSearchVisible});
    }

    filterData(searchText) {
        const {commits} = this.props;
        const searchRegex = new RegExp(searchText, 'i');
        const filteredData = _.filter(commits, commit => searchRegex.test(commit.message));

        return filteredData;
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        if (dif < 0) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {requestCommits, navigate} = this.props;
        const {isSearchVisible, searchText} = this.state;

        const commits = this.filterData(searchText);

        return (
            <AppContainer>
                {isSearchVisible && <TextInput
                    autoFocus={true}
                    placeholder={'Search'}
                    style={{...mainInputStyle, marginVertical: 10, marginHorizontal: 10}}
                    underlineColorAndroid={'transparent'}
                    value={searchText}
                    onChangeText={text => this.setState({searchText: text})}/>}
                {!_.size(commits) && <EmptyData/>}
                <FlatList
                    ListFooterComponent={<LoadMore entityName={'commits'} onPress={() => requestCommits(false)}/>}
                    keyExtractor={item => `${item.id}`}
                    refreshing={false}
                    onRefresh={() => requestCommits(true)}
                    onScroll={e => this.onScroll(e)}
                    data={commits}
                    renderItem={({item}) => <SingleCommit commit={item} navigate={navigate}/>}/>
            </AppContainer>
        );
    }
}

function SingleCommit({commit, navigate}) {
    const {message, created_at, committer_name} = commit;

    return (
        <ItemList onPress={() => navigate('CommitDiff', {commit})}>
            <CircledImage name={committer_name}/>
            <View style={{flexDirection: 'column', paddingHorizontal: 15, flex: 1}}>
                <FormattedText>{message}</FormattedText>
                <FormattedText style={{fontSize: 14, fontStyle: 'italic'}}>{getLastActivity(created_at)}</FormattedText>
            </View>
        </ItemList>
    );
}

const Commits = connect(state => ({commits: getCommits(state), previousRouteName: getPreviousRouteName(state)}), {
    requestCommits,
    clearCommits,
    setHeaderRightButton,
    setHeaderLeftButton,
    setFooter,
    navigate,
    setPagination
})(CommitsComponent);
registerRoute({Commits});
