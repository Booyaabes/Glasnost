import {registerReducer} from 'store';

const pipelines = {
    PIPELINES_RECEIVED: (state, {projectId, data}) => ({...state, pipelines: {...state.pipelines, [projectId]: data}}),
    CLEAR_PIPELINES: state => ({...state, pipelines: {}}),
    JOBS_RECEIVED: (state, {pipelineId, data}) => ({...state, jobs: {...state.jobs, [pipelineId]: data}})
};

registerReducer('pipelines', pipelines);
