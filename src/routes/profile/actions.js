import _ from 'lodash';
import {get} from 'api';

import {getRepositories} from '../repositories/selectors';
import {requestRepositoryApiCall} from '../repository/actions';
import {getMe} from './selectors';

export function requestMe(loader = true) {
    return async (dispatch) => {
        const {data} = await get({path: 'user', loader});

        dispatch({type: 'USER_RECEIVED', data});
    };
}

export function requestUser(id) {
    return async (dispatch) => {
        const {data} = await get({path: `users/${id}`});

        dispatch({type: 'OTHER_GUY_RECEIVED', data});
    };
}

export function requestEvents(userId) {
    return async (dispatch, getState) => {
        const me = getMe(getState());
        const {data} = await get({path: userId ? `users/${userId}/events` : 'events', loader: false});

        if (_.size(data)) {
            if (userId === me.id) {
                dispatch({type: 'EVENTS_RECEIVED', data});
            }
            dispatch({type: 'OTHER_GUY_EVENTS_RECEIVED', data});
        }
    };
}

function actionsMapping({action_name, target_type, target_title, push_data, note}) {
    const mappings = {
        opened: `Opened ${target_type}: ${target_title}`,
        created: 'Created a project',
        joined: 'Joined the project',
        accepted: `Accepted ${target_type}: ${target_title}`,
        closed: `Closed ${target_type}: ${target_title}`,
        deleted: `Deleted ${_.get(push_data, 'ref_type')}: ${_.get(push_data, 'ref')}`,
        'pushed new': `Created ${_.get(push_data, 'ref_type')}: ${_.get(push_data, 'ref')}`,
        'pushed to': `Pushed to ${_.get(push_data, 'ref_type')}: ${_.get(push_data, 'ref')}`,
        'commented on': `Commented on ${target_title}: ${_.get(note, 'body')}`
    };

    return mappings[action_name] || 'Undefined';
}

export function getActionMessage(event) {
    return async (dispatch, getState) => {
        const {project_id} = event;
        const repositories = getRepositories(getState());
        const repository = _.find(repositories, {id: project_id}) || _.get((await requestRepositoryApiCall(project_id, false)), 'data');

        return {message: actionsMapping(event), repoName: _.get(repository, 'name_with_namespace', 'Undefined')};
    };
}
