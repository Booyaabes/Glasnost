import {View} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import FingerprintScanner from 'react-native-fingerprint-scanner/src';

import {AppContainer, FormattedText, MainButton, ScrollableAppContainer} from 'elements';
import {getGitlabUrl} from 'utils';
import {registerRoute} from 'router';

import {changeHost, checkAndSetFingerprint} from '../settings/actions';

function onSuccess() {
    return async (dispatch, getState) => {
        const currentUrl = getGitlabUrl(getState());

        dispatch(checkAndSetFingerprint(true));
        dispatch(changeHost(currentUrl));
    };
}

export async function hasFingerprintHardware() {
    try {
        const type = await FingerprintScanner.isSensorAvailable();

        return type === 'Fingerprint' || type === 'Biometrics';
    } catch (e) {
        return false;
    }
}

class HardwareLoginComponent extends Component {
    state = {suitable: false};

    componentDidMount() {
        this.checkHardwareType();
    }

    async checkHardwareType() {
        this.setState({suitable: await hasFingerprintHardware()});
    }

    render() {
        const {url, changeHost, onSuccess, onSkip} = this.props;
        const {suitable} = this.state;
        const onSkipAction = () => (onSkip || changeHost(url));

        if (!suitable) {
            return <AppContainer/>;
        }

        return (
            <ScrollableAppContainer>
                <FormattedText style={{fontSize: 18}}>
                    {'It looks like your phone is able to scan fingerprints.'}
                </FormattedText>
                <FormattedText style={{fontSize: 18}}>
                    {'Would you like to add one more security layer on top of the normal login?'}
                </FormattedText>
                <View style={{marginTop: 30, flexDirection: 'row', justifyContent: 'space-around'}}>
                    <MainButton secondary={true} text={'Skip'} onPress={onSkipAction}/>
                    <MainButton text={'Use fingerprint'} onPress={onSuccess}/>
                </View>
            </ScrollableAppContainer>
        );
    }
}

const HardwareLogin = connect(state => ({url: getGitlabUrl(state)}), {changeHost, onSuccess})(HardwareLoginComponent);
registerRoute({HardwareLogin});
