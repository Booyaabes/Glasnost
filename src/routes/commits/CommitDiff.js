import React from 'react';

import {registerRoute} from 'router';
import {ScrollableAppContainer} from 'elements';

import {Diffs} from '../mergeRequest/Diffs';

function CommitDiff({commit}) {
    return (
        <ScrollableAppContainer>
            <Diffs commit={commit}/>
        </ScrollableAppContainer>
    );
}

registerRoute({CommitDiff});
