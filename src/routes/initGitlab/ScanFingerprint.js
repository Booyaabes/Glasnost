import {View, Platform} from 'react-native';
import React, {Component} from 'react';
import FingerprintScanner from 'react-native-fingerprint-scanner';

import {FormattedText, Icon, ScrollableAppContainer, colors, MainButton, CircledImage} from 'elements';
import {showMessage} from 'utils';
import {registerRoute} from 'router';

class ScanFingerprint extends Component {
    state = {success: false};

    componentDidMount() {
        this.authenticate();
    }

    authenticate() {
        if (this.requiresLegacyAuthentication()) {
            this.authLegacy();
        } else {
            this.authCurrent();
        }
    }

    requiresLegacyAuthentication() {
        return Platform.Version < 23;
    }

    componentWillUnmount() {
        FingerprintScanner.release();
    }

    async authLegacy() {
        const {onSuccess} = this.props;

        FingerprintScanner
            .authenticate({onAttempt: () => this.setState({success: false})})
            .then(() => {
                showMessage({message: 'Authenticated successfully'});
                this.setState({success: true});
                onSuccess();
            })
            .catch(async () => {
                this.setState({success: false});
                await this.authenticate();
            });
    }

    async authCurrent() {
        const {onSuccess} = this.props;

        try {
            await FingerprintScanner.authenticate({onAttempt: () => this.setState({success: false})});
            showMessage({message: 'Authenticated successfully'});
            this.setState({success: true});
            onSuccess();
        } catch (_e) {
            this.setState({success: false});
            await this.authenticate();
        }
    }

    render() {
        const {onSkip, skippable} = this.props;
        const {success} = this.state;

        return (
            <ScrollableAppContainer>
                <View style={{margin: 70, flexDirection: 'column', alignItems: 'center'}}>
                    <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: 50}}>
                        <CircledImage local={true} source={require('../../../assets/logo.png')} size={120}/>
                    </View>
                    <FormattedText
                        style={{fontSize: 18}}>{'Touch the fingerprint sensor for a few moments'}</FormattedText>
                    <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
                        {success && <Icon name={'status_success_borderless'} color={colors.success} size={80}/>}
                    </View>
                    {skippable && <MainButton text={'Skip'} onPress={onSkip}/>}
                </View>
            </ScrollableAppContainer>
        );
    }
}

registerRoute({ScanFingerprint});
