import React from 'react';
import {FlatList, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {AppContainer, Card, colors, Markdown} from 'elements';
import {getDiscussions, getIssue} from './selectors';

function DiscussionsComponent({issue, discussions}) {
    if (_.isEmpty(discussions)) {
        return <AppContainer/>;
    }

    const webUrl = issue.web_url || '';
    const baseUrl = (webUrl.match(/(.+)\/issues\/\d+/) || [])[1];

    return (
        <View style={{marginTop: 20}}>
            <FlatList
                keyExtractor={item => `${item.id}`}
                data={discussions}
                renderItem={({item}) => <SingleDiscussion discussion={item} baseUrl={baseUrl}/>}/>
        </View>
    );
}

export function SingleDiscussion({discussion, baseUrl}) {
    if (_.isEmpty(discussion)) {
        return null;
    }

    return (
        <View style={{marginTop: 10, justifyContent: 'flex-start', flex: 1}}>
            {_.map(discussion.notes, (note, index) => {
                const {resolved, resolvable, system} = note;
                const authorUsername = _.get(note, 'author.username', '');
                const body = _.get(note, 'body', '');
                const iconColor = resolved ? colors.success : colors.gray1;

                const icon = {color: iconColor, name: system ? 'tanuki' : 'status_success_borderless'};

                return (
                    <Card key={index} title={authorUsername} icon={!resolvable && !system ? null : icon}>
                        <Markdown text={body} baseUrl={baseUrl}/>
                    </Card>
                );
            })}
        </View>
    );
}

export const Discussions = connect(state => ({issue: getIssue(state), discussions: getDiscussions(state)}))(DiscussionsComponent);
