import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Switch} from 'react-native';

import {Card, FormattedText} from 'elements';

import {hasFingerprintHardware} from '../initGitlab';
import {getFingerprintValue} from './selectors';
import {checkAndSetFingerprint} from './actions';

class FingerprintComponent extends Component {
    state = {shouldShow: false};

    async componentDidMount() {
        this.setState({shouldShow: await hasFingerprintHardware()});
    }

    render() {
        const {hasFingerprint, checkAndSetFingerprint} = this.props;

        if (!this.state.shouldShow) {
            return null;
        }

        return (
            <Card style={{content: {flexDirection: 'row', justifyContent: 'space-between'}}} title={'Fingerprint'}
                icon={{name: 'eye-slash'}}>
                <FormattedText style={{fontSize: 18}}>Request the fingerprint scan:</FormattedText>
                <Switch value={hasFingerprint} onValueChange={checkAndSetFingerprint}/>
            </Card>
        );
    }
}

export const Fingerprint = connect(state => ({hasFingerprint: getFingerprintValue(state)}), {checkAndSetFingerprint})(FingerprintComponent);
