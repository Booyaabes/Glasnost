import React from 'react';
import {Provider} from 'react-redux';

import {getRegisteredRoutes, getRouteName} from 'router';

describe('Logout', () => {
    let Logout;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        Logout = getRegisteredRoutes().Logout;

        const {getStore} = require('store');
        store = getStore();
    });

    it('renders correctly the page', async () => {
        const component = await asyncCreate(<Provider store={store}><Logout/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('navigates to AddGitlabUrl when clicking the button', async () => {
        const component = mount(<Provider store={store}><Logout/></Provider>);
        const Button = component.find('MainButton');
        Button.props().onPress();

        component.update();
        await waitForAsync();

        expect(getRouteName(store.getState())).toEqual('AddGitlabUrl');
    });
});
