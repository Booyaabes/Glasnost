import _ from 'lodash';
import React from 'react';
import {connect} from 'react-redux';

import {CircledImage} from 'elements/circledImage';
import {toggleDrawerStatus} from 'headerFooter/actions';
import {getMe} from '../profile/selectors';

function MyFaceComponent({me, toggleDrawerStatus, size = 40}) {
    const avatar = _.get(me, 'avatar_url');
    const name = _.get(me, 'name');

    return (avatar || name) ? <CircledImage name={name} url={avatar} size={size} onPress={toggleDrawerStatus}/> : null;
}

export const MyFace = connect(state => ({me: getMe(state)}), {toggleDrawerStatus})(MyFaceComponent);
