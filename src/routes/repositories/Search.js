import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {TouchableOpacity, View} from 'react-native';

import {colors, FormattedText, Icon, SimpleInput} from 'elements';

import {isLocalSearch} from './selectors';
import {onlineSearch, setLocalSearch} from './actions';

class SearchComponent extends Component {
    reportChange = _.debounce(value => this.props.onlineSearch(value), 400);

    handleChange(value) {
        this.reportChange(value);
    }

    render() {
        const {value, localSearch, searchRemote = true, onChangeText, setLocalSearch, onlineSearch} = this.props;

        return (
            <View>
                {searchRemote && <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 10}}>
                    <TouchableOpacity
                        style={{flexDirection: 'row', alignItems: 'center'}}
                        onPress={() => setLocalSearch(true) && onChangeText(value)}
                    >
                        {localSearch && <Icon name={'mobile-issue-close'} size={20} color={colors.success}/>}
                        <FormattedText>Local search</FormattedText>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{flexDirection: 'row', alignItems: 'center'}}
                        onPress={() => setLocalSearch(false) && onlineSearch(value)}
                    >
                        {!localSearch && <Icon name={'mobile-issue-close'} size={20} color={colors.success}/>}
                        <FormattedText>Remote Search</FormattedText>
                    </TouchableOpacity>
                </View>}
                <View style={{flexDirection: 'row'}}>
                    <SimpleInput
                        autoFocus={true}
                        placeholder={'Search'}
                        style={{flex: 1, flexDirection: 'row'}}
                        value={value}
                        onChangeText={(text) => {
                            onChangeText(text);

                            if (!localSearch && searchRemote) {
                                this.handleChange(text);
                            }
                        }}/>
                </View>
            </View>
        );
    }
}

export const Search = connect(state => ({localSearch: isLocalSearch(state)}), {setLocalSearch, onlineSearch})(SearchComponent);
