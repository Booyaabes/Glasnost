import _ from 'lodash';
import {createSelector} from 'reselect';

const getBranchesSelector = state => _.get(state, 'branches');
const getCurrentBranch = state => _.get(state, 'branches.branch');
export const getBranches = createSelector(getBranchesSelector, branches => _.get(branches, 'branches'));
export const getDefaultBranch = createSelector(getBranchesSelector, branches => _.find(branches.branches, {default: true}));
export const getBranchName = createSelector(
                                        [getDefaultBranch, getCurrentBranch],
                                        (defaultBranch, currentBranch) => currentBranch || _.get(defaultBranch, 'name')
);
