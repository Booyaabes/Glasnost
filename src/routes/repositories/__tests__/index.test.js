import {getRegisteredRoutes} from 'router';

import * as index from '../index';

describe('Repositories index', () => {
    it('should export Repositories', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Repositories).toBeDefined();
    });
});
