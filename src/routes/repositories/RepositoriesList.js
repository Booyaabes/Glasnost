import React, {Component, memo} from 'react';
import {FlatList} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {MainFooter, setFooter, setHeaderLeftButton, setHeaderRightButton, setTitle} from 'headerFooter';
import {AppContainer, Icon, LoadMore} from 'elements';
import {setPagination} from 'utils';

import {checkDonation, checkStars, requestOnlineRepositories, setLocalSearch} from './actions';
import {getOnlineRepositories, isLocalSearch} from './selectors';
import {MyFace} from './MyFace';
import {Search} from './Search';
import {SingleRepository} from './SingleRepository';

class RepositoriesListComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {searchText: '', isSearchVisible: false, filteredData: []};
    }

    componentDidMount() {
        const {
            checkDonation,
            checkStars,
            setTitle,
            setFooter,
            setHeaderRightButton,
            setHeaderLeftButton
        } = this.props;

        setTitle('Projects');
        setFooter(this.footer());
        setHeaderRightButton(this.headerRightButton());
        setHeaderLeftButton(this.headerLeftButton());
        checkDonation();
        checkStars();
    }

    componentWillUnmount() {
        let {entityName} = this.props;
        const {localSearch, setLocalSearch, setPagination} = this.props;
        const {isSearchVisible} = this.state;

        if (!localSearch && isSearchVisible) {
            entityName = 'onlineRepositories';
        }

        setLocalSearch(true);
        setPagination(entityName, {'x-page': 0});
    }

    footer() {
        return <MainFooter/>;
    }

    headerRightButton() {
        return <Icon name={'search'} style={{padding: 5}} size={30} onPress={() => this.toggleSearch()}/>;
    }

    headerLeftButton() {
        return <MyFace/>;
    }

    toggleSearch() {
        const {isSearchVisible} = this.state;
        this.setState({isSearchVisible: !isSearchVisible});
    }

    filterData(searchText) {
        const {repositories} = this.props;
        const searchRegex = new RegExp(searchText, 'i');

        return _.filter(repositories, repo => searchRegex.test(repo.name));
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        if (dif < 0) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {isSearchVisible, searchText} = this.state;
        const {repositories, searchRemote, refresh = () => ({}), onlineRepositories, localSearch, requestOnlineRepositories} = this.props;
        let {entityName, onPress} = this.props;
        let data = repositories;

        if (localSearch && isSearchVisible) {
            data = this.filterData(searchText);
        } else if (!localSearch && isSearchVisible) {
            data = onlineRepositories;
            entityName = 'onlineRepositories';
            onPress = requestOnlineRepositories;
        }

        return (
            <AppContainer>
                {isSearchVisible
                && <Search
                    searchRemote={searchRemote}
                    value={searchText}
                    onChangeText={text => this.setState({searchText: text})}
                />}
                <FlatList
                    onRefresh={refresh}
                    refreshing={false}
                    onScroll={e => this.onScroll(e)}
                    ListFooterComponent={<LoadMore entityName={entityName} onPress={onPress}/>}
                    data={data}
                    keyExtractor={item => `${item.id}`}
                    renderItem={({item}) => <SingleRepository repo={item}/>}
                />
            </AppContainer>
        );
    }
}

export const RepositoriesList = connect(state => ({
    onlineRepositories: getOnlineRepositories(state),
    localSearch: isLocalSearch(state)
}), {
    checkDonation,
    checkStars,
    setFooter,
    setHeaderRightButton,
    setHeaderLeftButton,
    setLocalSearch,
    setTitle,
    requestOnlineRepositories,
    setPagination
})(memo(RepositoriesListComponent));
