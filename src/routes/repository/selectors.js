import _ from 'lodash';
import {createSelector} from 'reselect';

const getRepositorySelector = state => _.get(state, 'repository');
export const getRepository = createSelector(getRepositorySelector, repository => _.get(repository, 'repository'));
export const getFileList = createSelector(getRepositorySelector, repository => _.get(repository, 'fileList'));
export const getMembers = createSelector(getRepositorySelector, repository => _.get(repository, 'members'));
export const getBlob = createSelector(getRepositorySelector, repository => _.get(repository, 'blob'));
export const getBlobName = createSelector(getRepositorySelector, repository => _.get(repository, 'blob.name'));
export const getReadme = createSelector(getRepositorySelector, repository => _.get(repository, 'readme'));
