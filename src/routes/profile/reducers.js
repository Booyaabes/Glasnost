import {registerReducer} from 'store';

function profile() {
    return {
        USER_RECEIVED: (state, {data}) => ({...state, profile: data}),
        OTHER_GUY_RECEIVED: (state, {data}) => ({...state, otherGuyProfile: data}),
        EVENTS_RECEIVED: (state, {data}) => ({...state, events: data}),
        OTHER_GUY_EVENTS_RECEIVED: (state, {data}) => ({...state, otherGuyEvents: data})
    };
}

registerReducer('profile', profile());
