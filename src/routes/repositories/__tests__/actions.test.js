describe('actions', () => {
    let store;

    beforeEach(() => {
        store = require('store').getStore();
        store.dispatch({type: 'REPOSITORIES_RECEIVED', data: []});
    });

    it('checkStars should set now', async () => {
        const utils = require('utils');
        const {checkStars} = require('../actions');

        store.dispatch(await checkStars());

        expect(utils.getData).toHaveBeenCalledWith();
    });
});
