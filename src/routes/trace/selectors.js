import _ from 'lodash';

export const getTrace = (state, jobId) => _.get(state, ['trace', jobId], '');
