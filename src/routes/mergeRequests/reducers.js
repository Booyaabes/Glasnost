import {registerReducer} from 'store';
import _ from 'lodash';

const mergeRequests = {
    MERGE_REQUESTS_RECEIVED: (state, {data}) => ({...state, mergeRequests: _.uniqBy([...(state.mergeRequests || []), ...data], 'id')}),
    SET_MERGE_REQUESTS_PAGINATION: (state, {pagination}) => ({...state, pagination}),
    CLEAR_MERGE_REQUESTS: state => ({...state, mergeRequests: []})
};

registerReducer('mergeRequests', mergeRequests);
