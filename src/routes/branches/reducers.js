import {registerReducer} from 'store';

const branches = {
    BRANCHES_RECEIVED: (state, {data}) => ({...state, branches: data}),
    CURRENT_BRANCH: (state, {branch}) => ({...state, branch})
};

registerReducer('branches', branches);
