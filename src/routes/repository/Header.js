import React from 'react';
import {TouchableOpacity, View} from 'react-native';

import {CircledImage, FormattedText, Markdown} from 'elements';
import {getLastActivity, navigateFromUrl} from 'utils';

import {connect} from 'react-redux';
import {getRepository} from './selectors';

function Fork({forked_from_project, navigateFromUrl}) {
    return (
        <TouchableOpacity onPress={() => navigateFromUrl({url: forked_from_project.http_url_to_repo})}>
            <FormattedText
                style={{fontSize: 14}}>{`Forked from: ${forked_from_project.name_with_namespace}`}</FormattedText>
        </TouchableOpacity>
    );
}

function HeaderComponent({repository, navigateFromUrl}) {
    const {name_with_namespace, name, description, last_activity_at, avatar_url, forked_from_project} = repository;
    const formattedLastActivity = getLastActivity(last_activity_at);

    return (
        <View style={{
            alignItems: 'center', justifyContent: 'center', paddingVertical: 10, flexDirection: 'row', flex: 1
        }}>
            <View style={{flex: 0.2, marginHorizontal: 10}}>
                <CircledImage name={name} url={avatar_url} size={70}/>
            </View>
            <View style={{marginHorizontal: 10, flex: 0.8}}>
                <FormattedText style={{fontSize: 20, fontWeight: 'bold'}}>{name_with_namespace}</FormattedText>
                <Markdown text={description} style={{text: {fontSize: 14, fontStyle: 'italic'}}}/>
                <View style={{marginBottom: 10}}>
                    <FormattedText style={{fontSize: 14}}>{`Last activity: ${formattedLastActivity}`}</FormattedText>
                    {!!forked_from_project && <Fork forked_from_project={forked_from_project} navigateFromUrl={navigateFromUrl}/>}
                </View>
            </View>
        </View>
    );
}

export const Header = connect(state => ({repository: getRepository(state)}), {navigateFromUrl})(HeaderComponent);
