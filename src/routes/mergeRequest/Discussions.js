import React from 'react';
import {FlatList, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {getDiscussions, getMergeRequest} from './selectors';
import {SingleDiscussion} from '../issue/Discussions';

function DiscussionsComponent({mr, discussions}) {
    if (_.isEmpty(discussions)) {
        return null;
    }

    const webUrl = mr.web_url || '';
    const baseUrl = (webUrl.match(/(.+)\/merge_requests\/\d+/) || [])[1];
    const authorId = _.get(mr, 'author.id');
    const comments = _.filter(discussions, discussion => !(discussion.individual_note && discussion.notes[0].author.id === authorId));

    return (
        <View style={{marginTop: 20}}>
            <FlatList
                keyExtractor={item => `${item.id}`}
                data={comments}
                renderItem={({item}) => <SingleDiscussion discussion={item} baseUrl={baseUrl}/>}/>
        </View>
    );
}

export const Discussions = connect(state => ({mr: getMergeRequest(state), discussions: getDiscussions(state)}))(DiscussionsComponent);
