import _ from 'lodash';
import {registerReducer} from 'store';

const repositories = {
    REPOSITORIES_RECEIVED: (state, {data}) => ({...state, repositories: _.uniqBy([...(state.repositories || []), ...data], 'id')}),
    SET_REPOSITORIES_PAGINATION: (state, {pagination}) => ({...state, pagination}),
    CLEAR_REPOSITORIES: state => ({...state, repositories: []}),
    SET_LOCAL_SEARCH: (state, {localSearch}) => ({...state, localSearch})
};

const onlineRepositories = {
    ONLINE_REPOSITORIES_RECEIVED: (state, {data}) => ({...state, onlineRepositories: _.uniqBy([...(state.onlineRepositories || []), ...data], 'id')}),
    SET_ONLINEREPOSITORIES_PAGINATION: (state, {pagination}) => ({...state, pagination}),
    CLEAR_ONLINE_REPOSITORIES: state => ({...state, onlineRepositories: []}),
    SET_LAST_SEARCH_ONLINE: (state, {lastSearchOnline}) => ({...state, lastSearchOnline})
};

const starredRepositories = {
    STARRED_REPOSITORIES_RECEIVED: (state, {data}) => ({...state, starredRepositories: _.uniqBy([...(state.starredRepositories || []), ...data], 'id')}),
    SET_STARREDREPOSITORIES_PAGINATION: (state, {pagination}) => ({...state, pagination}),
    CLEAR_STARRED_REPOSITORIES: state => ({...state, starredRepositories: []})
};

registerReducer('repositories', repositories);
registerReducer('onlineRepositories', onlineRepositories);
registerReducer('starredRepositories', starredRepositories);
