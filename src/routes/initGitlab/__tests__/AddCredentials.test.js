import React from 'react';
import {Provider} from 'react-redux';

import {getNewGitlabUrl} from 'utils';
import {getRegisteredRoutes, getRouteName} from 'router';

describe('AddCredentials', () => {
    let AddCredentials;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        AddCredentials = getRegisteredRoutes().AddCredentials;

        const {getStore} = require('store');
        store = getStore();
    });

    it('should render correctly the AddCredentials page', async () => {
        const component = await asyncCreate(<Provider store={store}><AddCredentials/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should call the functions on unmount', async () => {
        const utils = require('utils');
        store.dispatch(utils.setNewUrl('https://someGitlabUrl.com'));
        const component = mount(<Provider store={store}><AddCredentials/></Provider>);

        component.unmount();

        expect(getNewGitlabUrl(store.getState())).toEqual('');
    });

    it('should render correctly the different url', async () => {
        const component = mount(<Provider store={store}><AddCredentials/></Provider>);
        const SimpleInput = component.find('SimpleInputComponent');

        SimpleInput.props().onChangeText('someAccessToken');
        SimpleInput.props().value = 'someAccessToken';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should render correctly the info helper onPress()', async () => {
        const component = mount(<Provider store={store}><AddCredentials/></Provider>);
        const Icon = component.find('IconComponent');

        Icon.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should change the gitlabUrl onPress()', async () => {
        const component = mount(<Provider store={store}><AddCredentials/></Provider>);
        const MainButton = component.find('MainButton').at(0);

        MainButton.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('AddGitlabUrl');
    });

    it('should open the browser onPress()', async () => {
        const component = mount(<Provider store={store}><AddCredentials/></Provider>);
        const Icon = component.find('IconComponent');

        Icon.props().onPress();
        component.update();

        const MainButton = component.find('MainButton').at(0);
        MainButton.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should display an error because the token is not valid', async () => {
        const utils = require('utils');
        store.dispatch(utils.setNewUrl('https://someGitlabUrl.com'));

        const component = mount(<Provider store={store}><AddCredentials/></Provider>);
        const SimpleInput = component.find('SimpleInputComponent');
        SimpleInput.props().onChangeText('someAccessToken');
        SimpleInput.props().value = 'someAccessToken';
        component.update();
        const MainButton = component.find('MainButton').at(1);

        MainButton.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
        expect(utils.showMessage).toHaveBeenCalledWith({error: true, message: 'Invalid Token!'});
    });

    it('should call the validLogin function', async () => {
        const utils = require('utils');
        store.dispatch(utils.setNewUrl('https://someGitlabUrl.com'));

        onGet({url: /version*/, data: {}});

        const component = mount(<Provider store={store}><AddCredentials/></Provider>);
        const SimpleInput = component.find('SimpleInputComponent');
        SimpleInput.props().onChangeText('someAccessToken');
        SimpleInput.props().value = 'someAccessToken';
        component.update();
        const MainButton = component.find('MainButton').at(1);

        MainButton.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
        expect(utils.showMessage).toHaveBeenCalledWith({message: 'Successfully logged in!'});
    });
});
