import _ from 'lodash';
import {registerReducer} from 'store';

const issues = {
    ISSUES_RECEIVED: (state, {data}) => ({...state, issues: _.uniqBy([...(state.issues || []), ...data], 'id')}),
    SET_ISSUES_PAGINATION: (state, {pagination}) => ({...state, pagination}),
    CLEAR_ISSUES: state => ({...state, issues: []})
};

registerReducer('issues', issues);
