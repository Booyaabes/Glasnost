import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes} from 'router';

const diffs = {
    diffs: [
        {
            renamed_file: 'renamedFile',
            new_path: 'newPath',
            diff: `
            diff --git a/setupTest.js b/setupTest.js
index 4b05446..a706a26 100644
--- a/setupTest.js
+++ b/setupTest.js
@@ -33,3 +33,17 @@ beforeEach(() => {
     require('./App');
 });
 
+function suppressDomErrors() {
+    const suppressedErrors = /(attribute \`accessible\`|The tag.*is unrecognized in this browser|PascalCase)/;
+    // eslint-disable-next-line no-console
+    const realConsoleError = console.error;
+    // eslint-disable-next-line no-console
+    console.error = message => {
+        if (message.match(suppressedErrors)) {
+            return;
+        }
+        realConsoleError(message);
+    };
+}
+
+suppressDomErrors();
\\ No newline at end of file

            `
        },
        {
            old_path: 'oldPath'
        }
    ]
};

const commit = {id: 3, created_at: '2019-05-11T14:30:23.406Z', message: 'someMessage3', committer_name: 'committerName3'};

describe('CommitDiff', () => {
    let CommitDiff;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        require('../../mergeRequest/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        CommitDiff = getRegisteredRoutes().CommitDiff;

        const {getStore} = require('store');
        store = getStore();

        onGet({url: /compare*/, data: diffs});
        onGet({url: /diff*/, data: diffs.diffs});
    });

    it('renders correctly the closed diff', async () => {
        const component = await asyncCreate(<Provider store={store}><CommitDiff/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('shows the MR changed files', async () => {
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: {project_id: 1}});

        const component = mount(<Provider store={store}><CommitDiff/></Provider>);

        const TouchableOpacity = component.find('TouchableOpacity');
        TouchableOpacity.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('shows the MR diffs', async () => {
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: {project_id: 1}});

        const component = mount(<Provider store={store}><CommitDiff/></Provider>);

        const TouchableOpacity = component.find('TouchableOpacity');
        TouchableOpacity.props().onPress();
        await waitForAsync();
        component.update();

        const TouchableDiff = component.find('DiffComponent').at(0).find('TouchableOpacity').at(0);
        TouchableDiff.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('shows the Commit changed files', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        const component = await asyncCreate(<Provider store={store}><CommitDiff commit={commit}/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('doesn\'t show anything if the diffs are empty', async () => {
        onGet({url: /diff*/, data: null});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        const component = await asyncCreate(<Provider store={store}><CommitDiff commit={commit}/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('shows the Commit diffs', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        const component = mount(<Provider store={store}><CommitDiff commit={commit}/></Provider>);

        const TouchableDiff = component.find('DiffComponent').at(0).find('TouchableOpacity').at(0);
        TouchableDiff.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});
