import _ from 'lodash';

export const getPipeline = (state, repoId, branch = 'master') => {
    const repoPipelines = _.get(state.pipelines, ['pipelines', repoId]);
    return _.maxBy(_.filter(repoPipelines, {ref: branch}), 'id');
};

export const getJobs = (state, pipelineId) => _.get(state.pipelines, ['jobs', pipelineId]);
